package com.jt.auth.config;


import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;


/**
 * 定义安全配置类
 * spring security 配置类，此类中要配置：
 * 1）加密对象
 * 2）配置认证规则
 * 当我们在执行登陆操作时，底层逻辑：(了解)
 * 1）Filter(过滤器)
 * 2）AuthenticationManager（认证管理器）
 * 3）AuthenticationProvider(认证服务处理器)
 * 4）UserDetailsService(负责用户信息的获取及封装)
 * @author Administrator
 * */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * 定义认证管理器对象，这个对象负责完成用户信息的认证，
     * 即判定用户身份信息的合法性，在基于oauth2协议完成认
     * 证时，需要此对象，所以这里讲此对象拿出来交给spring管理
     * @return
     * @throws Exception
     */
    @Bean
    public AuthenticationManager authenticationManagerBe() throws Exception {
        return super.authenticationManagerBean();
    }

    /**初始化加密对象
     * 此对象提供了一种不可逆的加密方式，相对于md5更加安全*/
    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
    /**配置认证规则*/
    //下面这一段架构升级后可以不需要了
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //super.configure(http);所有请求都将进行认证，认证后才能访问
        //1.关闭(禁用)跨域攻击 不写会报403
        http.csrf().disable();
        // http.authorizeRequests().antMatchers("/other/addcart").authenticated();必须登陆其他资源是必须认证才能访问
        //2.放行所有资源的访问(后续可以基于选择放行或者认证)
        http.authorizeRequests()
                .anyRequest().permitAll();
        //3.定义登陆成功和失败以后处理逻辑(可选)
        //假如没有如下设置登陆的话报404
        http.formLogin()//这句话会对外暴露一个登陆路径
                .successHandler(successHandler())
                .failureHandler(failureHandler());
    }

    //定义认证成功处理器
    //登录成功以后返回json数据
    @Bean
    public AuthenticationSuccessHandler successHandler(){
        //        return new AuthenticationSuccessHandler() {
        //            @Override
        //            public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        //
        //            }
        //        };
        //lambda
        return (request,response,authentication) ->{
            //1.构建map对象,封装响应数据
            Map<String,Object> map=new HashMap<>();
            map.put("state",200);
            map.put("message","login ok");
            //2.将map对象写到客户端  将map对象转换为json对象字符串
            writeJsonToClient(response,map);
        };
    }
    //定义登录失败处理器
    @Bean
    public AuthenticationFailureHandler failureHandler(){
        return (request,response, exception)-> {
            //1.构建map对象,封装响应数据
            Map<String,Object> map=new HashMap<>();
            map.put("state",500);
            map.put("message","login failure");
            //2.将map对象写到客户端   将map对象转换为json对象字符串
            writeJsonToClient(response,map);
        };
    }
    // 将map对象转换为json字符串
    private void writeJsonToClient(HttpServletResponse response,
                                   Map<String,Object> map) throws IOException {
        //将map对象,转换为json
            String json = new ObjectMapper().writeValueAsString(map);
        //设置响应数据的编码方式
            response.setCharacterEncoding("utf-8");
        //设置响应数据的类型
            response.setContentType("application/json;charset=utf-8");
        //将数据响应到客户端
        PrintWriter out = response.getWriter();
        out.println(json);
        out.flush();
    }
}
