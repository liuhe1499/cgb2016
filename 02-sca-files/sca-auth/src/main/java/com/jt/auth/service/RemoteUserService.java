package com.jt.auth.service;

import com.jt.auth.pojo.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(value = "sca-system", contextId ="remoteUserService" )
public interface RemoteUserService {
    /**定义基于用户查询用户信息的方法*/
    @GetMapping("/user/login/{username}")
    User selectUserByUsername(
            @PathVariable("username") String username);

    /**基于用户名查询用户权限信息*/
    @GetMapping("/user/permission/{userId}")
    List<String> selectUserPermissions(
            @PathVariable("userId") Long userId);
}
