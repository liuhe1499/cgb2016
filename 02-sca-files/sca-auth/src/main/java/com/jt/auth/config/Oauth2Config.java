package com.jt.auth.config;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import java.util.Arrays;

/**
 * 在这个对象中负责将所有的认证和授权相关配置进行整合，例如
 * 业务方面：
 * 1）如何认证(认证逻辑的设计)
 * 2）认证通过如何颁发令牌(令牌的规范)
 * 3）为谁颁发令牌(客户端标识)
 * 技术方面：
 * 1)SpringSecurity (提供认证和授权的实现)
 * 2)TokenConfig(提供了令牌的生成，存储，校验)
 * 3)Oauth2(定义了一套认证规范，例如为谁发令牌，都发什么，...)
 *
 */
@AllArgsConstructor/**生成全参构造函数*/
@Configuration
@EnableAuthorizationServer/**开启认证和授权服务 @author liu*/
public class Oauth2Config extends AuthorizationServerConfigurerAdapter {

    /**此对象负责完成认证管理*/
    private AuthenticationManager authenticationManager;
    /**TokenStore负责完成令牌创建,信息读取*/
    private TokenStore tokenStore;
    /**JWT令牌转换器(基于用户信息构建令牌,解析令牌)*/
    private JwtAccessTokenConverter jwtAccessTokenConverter;
    /**密码加密匹配器对象*/
    private PasswordEncoder passwordEncoder;
    /**负责获取用户信息信息*/
    private UserDetailsService userDetailsService;


    /**
     * 认证细节配置
     * */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        /*super.configure(endpoints)*/
        endpoints
                /*由谁完成认证？*/
                //配置认证管理器
                .authenticationManager(authenticationManager)
                /*谁负责访问数据库？(认证时需要两部分信息：一部分来自客户端，一部分来自服务器)*/
                //验证用户的方法获得用户详情
                .userDetailsService(userDetailsService)
                /*支持对什么请求进行认证(默认支持post方式)*/
                //要求提交认证使用post请求方式,提高安全性
                .allowedTokenEndpointRequestMethods(HttpMethod.POST,HttpMethod.GET)
                //要配置令牌的生成,由于令牌生成比较复杂,下面有方法实现
                /*认证成功后令牌如何生成和存储？(默认令牌生成UUID.randomUUID(),存储方式为内存)*/
                .tokenServices(tokenService());
                //这个不配置,默认令牌为UUID.randomUUID().toString()
    }

    /**定义令牌生成策略
     * 系统底层在完成认证以后会调用TokenService对象的相关方法
     * 获取TokenStore，基于tokenStore获取token
     */
    @Bean
    public AuthorizationServerTokenServices tokenService(){
        //1.构建TokenService对象(此对象提供了创建，获取，刷新token的方法)
        //这个方法的目标就是获得一个令牌生成器
            DefaultTokenServices tokenServices=new DefaultTokenServices();
        //2.设置令牌生成和存储策略(tokenStore在TokenConfig配置了,本次我们应用JWT-定义了一种令牌格式)
            tokenServices.setTokenStore(tokenStore);
        //3.设置是否支持令牌刷新略(令牌有过期时间，是否支持令牌刷新机制，延长令牌有效期)
            tokenServices.setSupportRefreshToken(true);
        //4.设置令牌增强(默认令牌会比较简单，没有业务数据就是简单的随机字符串，但现在想要用JWT方式)
            TokenEnhancerChain tokenEnhancer=new TokenEnhancerChain();
            tokenEnhancer.setTokenEnhancers(Arrays.asList(jwtAccessTokenConverter));
            tokenServices.setTokenEnhancer(tokenEnhancer);
        //5.设置令牌有效期 1小时
            tokenServices.setAccessTokenValiditySeconds(3600);
        //6.刷新令牌应用场景：一般在用户登录系统后，令牌快过期时，系统自动帮助用户刷新令牌，提高用户的体验感 3天
            tokenServices.setRefreshTokenValiditySeconds(3600*72);
            return tokenServices;
    }

    /**
     * 客户端详情配置
     * 认证中心是否要给所有客户端发令牌呢？假如不是，那要给那些客户端发令牌，是否在服务端有一些规则的定义呢？
     * 例如：老赖不能坐飞机，不能坐高铁
     * @param clients
     * @throws Exception
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                //定义客户端id (客户端提交用户信息进行认证时需要这个id)
                .withClient("gateway-client")
                //客户端秘钥(客户端提交用户信息进行认证时需要携带这个密钥)
                .secret(passwordEncoder.encode("123456"))
                //定义作用范围(所有符合的客户端)   all只是个名字而已和写abc效果相同
                .scopes("all")
                //允许客户端基于密码方式，刷新令牌方式实现认证
                .authorizedGrantTypes("password","refresh_token");
    }
    /**
     * 假如我们要做认证，我们输入了用户名和密码，然后点提交
     * ，提交到哪里(url-去哪认证)，这个路径是否需要认证？还有令牌过期了，
     * 我们要重新生成一个令牌，哪个路径可以帮我们重新生成？
     * 如下这个方法就可以提供这个配置
     * @param security
     * @throws Exception
     */
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        //认证通过后,允许客户端进行哪些操作
        security
                /*1.定义(公开)要认证的url(permitAll()是官方定义好的)*/
                //公开oauth/token_key端点
                .tokenKeyAccess("permitAll()")
                /*2.定义(公开)令牌检查url*/
                //公开oauth/check_token端点
                .checkTokenAccess("permitAll()")
                /*3.允许客户端直接通过表单方式提交认证*/
                //允许提交请求进行认证(申请令牌)
                .allowFormAuthenticationForClients();
    }
}

