package com.jt.resource.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义注解:通过此注解描述需要执行拓展业务逻辑的方法，
 * 简单点就是做个标识
 *定义RequiredLog注解,通过此注解对需要进行日志记录的方法进行描述
 * @author Administrator*/
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RequiredLog {
    String value() default "";
    //........
}
