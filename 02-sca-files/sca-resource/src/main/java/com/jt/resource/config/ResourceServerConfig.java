package com.jt.resource.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * 思考：对于一个系统而言，他资源的访问权限你是如何进行分类设计的
 * 1）不需要登陆就可以访问(例如12306查票)
 * 2）登陆以后才能访问（例如12306的购票）
 * 3）登陆以后没有权限也不能访问（例如会员等级不够，不让执行一些相关操作）
 * */

/**
 * @author Administrator
 */
@Configuration
@EnableResourceServer
//启动方法上的权限控制，需要授权才可访问的方法上添加这样的@PreAuthorize等相关注解
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

//    @Autowired
//    private TokenStore tokenStore;
//
//    /**
//     * token服务配置
//     */
//    @Override
//    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
//        //通过TokenStore获取token解析器对象，基于此对象对token进行解析
//        resources.tokenStore(tokenStore);
//    }
    /**
     * 路由安全认证配置
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        //1.关闭跨域攻击
        http.csrf().disable();
        //2.放行相关请求
        http.authorizeRequests()
                //此访问路径必须登陆后才能上传
                .antMatchers("/resource/upload/**").authenticated()
                .anyRequest().permitAll();

//        http.exceptionHandling()
//                .accessDeniedHandler(accessDeniedHandler());


    }
    //没有权限时执行此处理器方法  ()
//    public AccessDeniedHandler accessDeniedHandler() {
//        return (request, response, e) -> {
//            Map<String, Object> map = new HashMap<>();
//            map.put("state", HttpServletResponse.SC_FORBIDDEN);//SC_FORBIDDEN的值是403
//            map.put("message", "没有访问权限,请联系管理员");
//            //1设置响应数据的编码
//            response.setCharacterEncoding("utf-8");
//            //2告诉浏览器响应数据的内容类型以及编码
//            response.setContentType("application/json;charset=utf-8");
//            //3获取输出流对象
//            PrintWriter out=response.getWriter();
//            //4 输出数据
//            String result=
//                    new ObjectMapper().writeValueAsString(map);
//            out.println(result);
//            out.flush();
//        };
//    }

}
