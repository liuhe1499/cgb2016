package com.jt.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.system.pojo.Log;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户行为日志数据层对象
 * @author Administrator
 */
@Mapper
public interface LogMapper extends BaseMapper<Log>{

}

