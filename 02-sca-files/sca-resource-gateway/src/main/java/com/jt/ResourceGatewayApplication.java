package com.jt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author liu
 * 2021/09/24
 */
@SpringBootApplication
public class ResourceGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(ResourceGatewayApplication.class,args);
    }
}
