package com.jt;

import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

/**
 * Redis中的事物测试
 * 什么是事物？事物是一个逻辑工作单元，是一个业务，一般要求这个事物中的所有操作要么都执行，要么都不执行(原子性)
 * 为什么要使用事物(使用事物的目的是什么)？保证数据的正确性(一致性)
 * 事物基于什么特性保证数据的正确？ACID
 * redis 中的实务指令：
 * 1）multi  手动开启事物
 * 2）discard  手动取消事物
 * 3）exec  手动提交事物
 */
public class JedisTransactionTests {
    @Test
    public void testTransaction(){
        //1.创建redis链接对象
        Jedis jedis = new Jedis("192.168.126.128",6379);
        //2.定义转账业务初始数据
        jedis.set("tony", "1000");
        jedis.set("jack", "800");
        //实现操作,tony转账100给jack
        //3.开启事物
        Transaction multi = jedis.multi();
        //3.1执行事物操作
        try {
            multi.decrBy("tony", 100);
            multi.incrBy("jack", 100);
            //模拟异常
            int a = 100/0;
        //提交事务
            multi.exec();
        }catch (Exception e){
            //出现异常取消事物
            multi.discard();
            //e.printStackTrace();
            throw new RuntimeException(e);
        }finally {
            //释放资源
            jedis.close();
        }
        String tonyMoney=jedis.get("tony");
        String jackMoney=jedis.get("jack");
        System.out.println("tonyMoney="+tonyMoney);
        System.out.println("jackMoney="+jackMoney);

    }
}
