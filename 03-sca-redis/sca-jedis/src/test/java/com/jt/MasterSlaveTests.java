package com.jt;

import org.junit.Test;
import redis.clients.jedis.Jedis;

/***
 * 主从架构测试分析
 */
public class MasterSlaveTests {
    //Mater节点(支持读写)
    @Test
    public void testWriteRead(){
        Jedis jedis = new Jedis("192.168.126.128",6379);
        jedis.set("a1", "100");
        String a1 = jedis.get("a1");
        System.out.println(a1);
        jedis.close();
    }
    //Slave节点(只允许读)
    @Test
    public void testRead(){
        Jedis jedis = new Jedis("192.168.126.128",6380);
        //jedis.set("a1", "100"); 这里不允许写
        String a1 = jedis.get("a1");
        System.out.println(a1);
        jedis.close();
    }
}
