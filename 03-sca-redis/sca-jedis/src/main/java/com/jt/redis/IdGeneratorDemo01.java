package com.jt.redis;

import redis.clients.jedis.Jedis;

/**
 * @author Administrator
 */
/**分布式id生成策略*/
public class IdGeneratorDemo01 {
    /**
     *编写一个方法，每次调用此方法外界都能获取一个唯一的递增整数值
     * */
    public static Long getId(){
        Jedis jedis = new Jedis("192.168.126.128", 6379);
        //jedis.auth("123456");有密码输入密码
        //incr方法用于对指定key的值进行递增，假如key不存在则创建
        Long id = jedis.incr("id");
        jedis.close();
        return id;
    }

    public static void main(String[] args) {
        System.out.println(getId());
        System.out.println(getId());
       for (int i=0;i<10;i++){
           new Thread() {
               @Override
               public void run() {
                   System.out.println(IdGeneratorDemo01.getId());
                   //用类名点方法是因为方法名和线程内部的方法重叠了
               }
           }.start();
       }
    }
}
