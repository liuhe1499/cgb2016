package com.jt.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

import java.util.List;

/**
 * 基于redis实现一个简单的多线程秒杀操作
 * 这个操作中重点演示一下乐观锁的应用
 * 乐观锁：允许多个线程同时对一条记录进行修改，但只能有一个线程修改成功
 * @author Administrator
 */
public class SecondKillDemo01 {

    public static void main(String[] args) throws InterruptedException {
        //建立连接
        Jedis jedis = new Jedis("192.168.126.128", 6379);
        //添加数据
        jedis.set("ticket", "10");
        jedis.set("money", "0");
        //添加线程

        Thread t1 = new Thread(()->{
            secKill();
        });
        Thread t2 = new Thread(()->{
            secKill();
        });

        t1.start();
        t2.start();

    }

    public static void secKill() {
        //建立连接
        Jedis jedis = new Jedis("192.168.126.128",6379);
        //对值进行观察
        jedis.watch("ticket","money");
        String ticket = jedis.get("ticket");
        //判断票数
        if (ticket==null || Integer.parseInt(ticket)==0) {
            throw new RuntimeException("已无库存");
        }
        //开启事务
        Transaction multi = jedis.multi();
        try {
            multi.decr("ticket");
            multi.incrBy("money", 100);
            List<Object> exec = multi.exec();
            System.out.println(exec);
        }catch (Exception e){
            //取消事物 抛出异常
            multi.discard();
            e.printStackTrace();
        }finally {
            //取消监控 释放资源
            jedis.unwatch();
            jedis.close();
        }
    }
}
