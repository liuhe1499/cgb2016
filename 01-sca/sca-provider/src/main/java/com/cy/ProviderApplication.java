package com.cy;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@SpringBootApplication
@Slf4j //日志注解
public class ProviderApplication {
    //private static final Logger log = LoggerFactory.getLogger(ProviderApplication.class);
    public static void main(String[] args) {
        SpringApplication.run(ProviderApplication.class,args);
    }

    //@RefreshScope 注解用于告诉spring。一般配置中心数据发生变化，自动重新创建它描述的类的实例
    @RefreshScope //动态刷新配置   对象
    @RestController
    public class ProviderController{

        @Value("${server.tomcat.threads.max:200}")
        private Integer maxThread;

        @RequestMapping("/provider/doGetMaxThread")
        public String doGetMaxThread(){
            return "server.threads.max is  "+maxThread;
        }

        //此属性会在对象构建时初始化
        //服务启动时会将所有配置信息(配置文件或配置中心)读取到Environment对象
        //@Value用于告诉spring从Environment读取配置信息
        //将读取到配置内容赋值给对象属性
        @Value("${logging.level.com.cy:debug}")
        private String logLevel;

        @GetMapping("/provider/level")
        public String doGetLevel(){
            //日志的输出会随着配置中心日志级别的更新进行调整
            log.trace("==log.trace==");//跟踪
            log.debug("==log.debug==");//调试
            log.info("==log.info==");//常规信息
            log.warn("==log.warn==");//警告
            log.error("==log.error==");//错误信息
            return "log level is " + logLevel;
        }

        /**
         * @Value用于读取application.yml中的配置
         * 要读取的配置文件需要写在${}中
         * ${}表达式中“：”后的内容为默认值
         * 8080为没有读到server.port的值时,给定的默认值
         * */
        @Value("${server.port:8080}")
        private String server;
        /**
        *基于此方法实现一个字符串的回显
        * echo:回显的意思
        *rest：一种软件架构编码风格，可以基于这种风格定义url
        * 访问: http://localhost:8081/provider/echo/nacos
        * */
        @GetMapping("/provider/echo/{msg}")
        public String doRestEcho1(@PathVariable("msg") String msg) throws InterruptedException {
            //System.out.println("====start=====");
            //Thread.sleep(5000);
            //level:error
            //需求：通过配置中心中动态日志级别配置，控制日志信息的输出
            //常用的日志级别： trace<debug<info<warn<error
            //很多系统的默认级别是info，调试程序时经常会使用debug，
            log.info("doRestEcho1 start {}",System.currentTimeMillis());//{} 表示占位符
            return server+" say hello " +msg;
        }
    }
}
